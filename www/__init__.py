__version__ = '2.0.1'
__description__ = '''BaoAI
小宝人工智能和量化平台
'''
def create_app():
    from . import modules
    modules.init_app()

__all__ = (
    '__version__',
    '__description__',
    'create_app'
)