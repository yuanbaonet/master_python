import os
import traceback
def init_app():
    '''init modules # 初始化自定义模块
    1. 动态的导入所有模块
    2.执行导入模块的init_app方法

    '''
    # 1 找出app/modules的所有子目录 （现在有两个 admin profiles）
    # __file__
    basedir = os.path.abspath(os.path.dirname(__file__))
    modules_dirs = []
    for root,dirs,files in os.walk(basedir):
        dirs.remove('__pycache__')
        modules_dirs = dirs
        break
    from importlib import import_module
    for module_name in modules_dirs: # modules_dirs = ['admin', 'profiles']
        try:
            import_module('.%s'%module_name, package=__name__).init_app()
        except Exception as e:
            print('%s module exception, traceback:\n%s'%(module_name, traceback.format_exc()))


