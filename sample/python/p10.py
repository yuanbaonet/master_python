fh = None
try:
    fh = open("testfile", "r+")
    fh.write("测试异常")
except IOError:
    print("ERROR:未找到文件")
else:
    print("测试正常")
finally:
    print("关闭文件")
    if fh:
       fh.close()

try:
    raise Exception("主动抛出错误")
    print("测试主动抛出错误")
except Exception as e:
    print("Error:%s"%e)
else:
    print("未出现异常")
finally:
    print("测试结束")
