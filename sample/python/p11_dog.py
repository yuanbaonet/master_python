class Dog(object):
    def __init__(self, name, age):
        self.__name = name
        self.__age = age

    def __str__(self):
        return "name:%s, age:%d" %(self.__name, self.__age)

    @property
    def name(self):
        return self.__name

    @name.setter # dog1.name = "旺财"
    def name(self, name):
        self.__name = name

class HabaDog(Dog):
    pass
