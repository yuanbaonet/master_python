# 打开文 件
f = open('test.txt', 'a+', encoding='utf-8')
print(f)
f.write('hello world')
f.close

# with
try:
    f = open('test.txt','r')
    print(f.read())
finally:
    if f:
        f.close()

with open('test.txt','r') as f :
    print(f.read())