# 创建类，生成对象
class Dog:
    # 初始化方法
    def __init__(self, name, age):
        self.name = name
        self.__age = age

    # 销毁方法
    def __del__(self):
        print('__del__')
        self.name=''
        self.age = 0

    # __str__ 返回一个字符串
    def __str__(self):
        return self.name + "," + str(self.__age)

    # 狗吃饭方法
    def eat(self):
        print('狗吃肉')

    # 狗叫的方法
    def bark(self):
        print('旺')

    # 打印狗信息
    def info(self):
        print("姓名：%s, 年龄：%d"%(self.name, self.age))

    # 设置狗的名字
    def set_name(self, name):
        self.name = name

    # 读取狗的名字
    def get_name(self):
        return self.name

    def get_age(self):
        return self.__age

# dog1 = Dog()
# print(dog1)

# 属性
#dog1.name = "旺财"
# dog1.set_name("旺财")
# dog1.age = 13
# dog1.sex = 'male'
# print(dog1.get_name())

# dog1.eat()
# dog1.info()

dog1 = Dog("旺财", 13)
print(dog1)
print(dog1.name)
print(dog1.get_age())

del dog1
print('over')

# 练习
class Cat:
    def __init__(self, name):
        self.name = name
        self.age = 0

    def yearPass(self):
        self.age += 1

    def __str__(self):
        info = self.name + ", " + str(self.age) + "岁" + ", "
        extend_info = '无饰物'
        if self.age >=1 and self.age <3 :
            pass
        elif self.age >=3 and self.age <5 :
            extend_info = "衣服"
        elif self.age >=5 and self.age <8 :
            extend_info = "衣服、脚垫"
        elif self.age >=8 and self.age <10 :
            extend_info = "衣服、脚垫、铃铛"
        else :
             extend_info = "衣服、脚垫、铃铛、剪胡子"
        return (info + extend_info)

cat1 = Cat('汤姆')
for i in range(1,12):
    cat1.yearPass()
    print(cat1)