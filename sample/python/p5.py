# if
score = 88

if score>=90 and score<=100 :
    print("优")
elif score<=90 and score>=80 :                 # else if
    print("中")
else :
    print("差")

# while
count = 0
while count<5 :
    # continue中止本次循环
    # 下面语句会导致永远循环
    # if count == 1:
    #     continue
    print('hello')
    count = count + 1

    # break中止循环
    if count==3 :
        break

else :
    print('exit while')

# for
lst = ['baidu', 'alibaba', 'tencent']
for value in lst :
    print(value)

for i in range(5) :
    print(i)

for i in range(7,0,-1):
    print(i)

# 习题 1
score = 55
if score >= 90 and score <= 100 :
    print("优秀")
elif score >= 80 and score < 90 :
    print("良好")
elif score >= 70 and score < 80 :
    print("中等")
elif score >= 60 and score < 70 :
    print("及格")
else :
    print("不及格")

# 习题2
for i in range(7,0,-1) :
    star = ''
    for j in range(i) :
       star = star + '*'
    print(star)

for i in range(7,0,-1) :
    star = '*'
    print(star*i)


