# 这是注释
'''
一行
二行
三行
'''

# # 缩进 ctrl + ]
# a = 5
# b = 6

# # 关键字
# import keyword
# print(keyword.kwlist, len(keyword.kwlist))

# # pass
# if a==5 :
#     pass

# # 多行语句
# c = a + \
#     b
# print(c)

# # 引号
# word = 'word'
# sentence = "这是一个句子"
# paragraph = """这是一个段落。
# 包含了多个语句。"""
# print(word, sentence, paragraph)

# # 用户输入
# print("How old are you?")
# age = input()
# print("so %s old"%age)

# 运算符
# 算术运行符
print(3**3)
a = 8
b = 3
print(a/b)
print(a//3)
print(a%b)

# 比较运算符
if a==8 :
    print("a==8")

# 赋值运算符
a += 1 # a = a + 1
print(a)

# 逻辑运算符
if a==9 or b==2 :
    print("ok")
else:
    print("error")

# 数据类型
value1 = 3
print(type(value1))

value2 = 3.14
print(type(value2))

value5 = "hello"
print(type(value5))

a = True
del a
print(a)
