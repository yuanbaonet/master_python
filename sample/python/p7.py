# 声明一个无参数函数
def sayHello() :
    print('hello world')
# 运行函数
sayHello()

# 声明一个带参数函数
def printValue(value) :
    print('参数为', value)
# 运行函数
printValue(9527)

# 返回值
# 1. 无返回值
def sum1(a,b):
    sum = a + b

result = sum1(3,8)
print(result)

# 2. 带返回值
def sum2(a,b):
    sum = a + b
    return sum

result = sum2(3,8)
print(result)

# 3. 返回多个值
def sum3(a,b):
    sum1 = a + b
    sum2 = a*b
    return sum1, sum2

result1, result2 = sum3(3,8)
print(result1, result2)

# 可变参数
def sum(*args) :
    print(args)
    result = 0
    for i in args :
        result += i
    return result

print(sum(1))
print(sum(1,2))
print(sum(1,2,3))
    
# 字典
def fun(value1, value2, value3=0, *args, **kwargs):
    print('value1=',value1, 'value2=', value2, 'value3=', value3, 'args=', args, 'kwargs=', kwargs)

fun(1,2)
fun(1,2,3)
fun(1,2,3,4)
fun(value1=4,value2=5,value3=6)
fun(1,2,'hello','world')
fun(1,2,3,name='zhansan',age=23,height=178.3)

# global
a = 10
def fun1():
    #value = a + 1
    global a
    a = a + 1
    # print(a,value)
    print(a)

fun1()

# 递归
# 5！ ＝ 5*4*3*2*1 ＝ 5*4！ ＝ 5*4*3！＝5*4*3*2！＝5*4*3*2*1！＝ 5*4*3*2*1 ＝
# n! = n*(n-1)!
# fac(n)=n*fac(n-1)
# fac(5) = 5*fac(4) = 5*4*fac(3) = 5*4*3*fac(2) = 5*4*3*2*fac(1) = 5*4*3*2*1
def fac(n):
    # 找出口
    if n==1 or n==0 :
        return 1
    # 进入下一层方式
    return n*fac(n-1)
print(fac(5))

# 匿名函数
add = lambda x,y: x+y
print(add(3,8))

# 习题
import random
def guess(value):
    print('请先想好一个值（1~100）:%s,按回车开始游戏'%value)
    input()
    start = 1
    end = 100
    while True:
        pcGuess = random.randint(start,end)
        print('电脑猜%s,用户反馈(1 大了 －1 小了 0 中了） ：'%pcGuess)
        result = int(input())
        if result == 1 :
            end = pcGuess - 1
        elif result == -1 :
            start = pcGuess + 1
        else :
            break
# 假设用户想的是78
guess(78)


